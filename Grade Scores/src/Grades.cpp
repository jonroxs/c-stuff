/*Jon Healy*/
/*Grade Average*/

#include <iostream>
#include <string>

using namespace std;

void clear()
	{int i;
	 for(i = 1; i <= 26; i++)
         cout << endl;
}


void scores(string name, int s1, int s2, int s3, int s4, int s5, int low, int total)
   {int average;
	total = total - low;
	average = total / 4;

	cout << "The average of test scores ";

	if(!(s1 == low))
		cout << s1 << " ";

	if(!(s2 == low))
		cout << s2 << " ";

	if(!(s3 == low))
		cout << s3 << " ";

	if(!(s4 == low))
		cout << s4 << " ";

	if(!(s4 == low))
		cout << s5 << " ";

	cout << " is " << average << " ";

	if(average >= 65) {
		cout << name << " is passing." << endl;
		cout << endl;
	}

	if(average < 65){
		cout << name << " is failing." << endl;
		cout << endl;
	}

}


void lowscore(string name, int s1, int s2, int s3, int s4, int s5, int total) {

	int low = 100;

	if(s1 < low)
		low = s1;

	if(s2 < low)
		low = s2;

	if(s3 < low)
		low = s3;

	if(s4 < low)
		low = s4;

	if(s5 < low)
		low = s5;

	scores(name, s1, s2, s3, s4, s5, low, total);
}


void name()
   {string name; 
	int s1, s2, s3, s4, s5, total = 0; 

	cout << "Enter the name of the student ";
	cin >> name;
	cout << endl;

	do{
		cout << "Enter test score 1 ";
		cin >> s1;
		total = total + s1;
		cout << endl;

		if(!((s1 >= 1) && (s1 <= 100))) {
			cout << "Grade value must be between 1 and 100!" << endl;
			cout << endl;
		}

	}while(!((s1 >= 1) && (s1 <= 100)));

	do{
		cout << "Enter test score 2 ";
		cin >> s2;
		total = total + s2;
		cout << endl;

		if(!((s2 >= 1) && (s2 <= 100))) {
			cout << "Grade value must be between 1 and 100!" << endl;
			cout << endl;
		}

	}while(!((s2 >= 1) && (s2 <= 100)));

	do{
		cout << "Enter test score 3 ";
		cin >> s3;
		total = total + s3;
		cout << endl;

		if(!((s3 >= 1) && (s3 <= 100))) {
			cout << "Grade value must be between 1 and 100!" << endl;
			cout << endl;
		}

	}while(!((s3 >= 1) && (s3 <= 100)));

	do{
		cout << "Enter test score 4 ";
		cin >> s4;
		total = total + s4;
		cout << endl;

		if(!((s4 >= 1) && (s4 <= 100))) {
			cout << "Grade value must be between 1 and 100!" << endl;
			cout << endl;
		}

	}while(!((s4 >= 1) && (s4 <= 100)));

	do{
		cout << "Enter test score 5 ";
		cin >> s5;
		total = total + s5;
		cout << endl;

		if(!((s5 >= 1) && (s5 <= 100))) {
			cout << "Grade value must be between 1 and 100!" << endl;
			cout << endl;
		}

	}while(!((s5 >= 1) && (s5 <= 100)));

	lowscore(name, s1, s2, s3, s4, s5, total);

}

int main() {

char ans;
do{
	clear();
	cout << "Grades" << endl;
	cout << endl;

	name();

	do{
	   	cout << "Would you like to enter another student? ";
	   	cin >> ans;
	   	cout << endl;

	   	if(!((ans == 'y') || (ans == 'Y') || (ans == 'n') || (ans == 'N'))) {
  		   	cout << "The choices are Y or N " << endl;
 			cout << endl;
	   	}

	}while(!((ans == 'y') || (ans == 'Y') || (ans == 'n') || (ans == 'N')));

}while((ans == 'y') || (ans == 'Y'));

return(0);
}
