/*Jon Healy*/
/*Food Order*/

#include <iostream>
#include <string>

using namespace std;

void clear(){
	 int i;
	 for(i = 1; i <= 26; i++)
	 	   cout << endl;
}

void Total(double total, int Bamount, int Famount, int Damount, char ans){
	double tax = .08;
	total = total + (total * tax);

	cout << "The total for the order with " << Bamount << " burgers, " << Famount << " fries, and ";
	cout << Damount << " drinks, is " << total << endl;
	cout << endl;
}

void drink(double total, int Bamount, int Famount, int Damount, char ans){
	double Dcost = 1.50;

	do{
		cout << "How many drinks?: "<< endl;
		cin >> Damount;
		cout << endl;

		if(Damount < 0){
			cout << "You cannot order a negative amount of drinks!" << endl;
			cout << endl;
		}

	}while(Damount < 0);

	total = total + (Damount * Dcost);
	Total(total, Bamount, Famount, Damount, ans);

}

void fries(double total, int Bamount, int Famount, int Damount, char ans){
	double Fcost = 1.75;

	do{
		cout << "How many orders of fries?: ";
		cin >> Famount;
		cout<< endl;

		if(Famount < 0){
			cout << "You cannot order a negative amount of fries!" << endl;
			cout << endl;
		}

	}while(Famount < 0);

	total = total + (Famount * Fcost);

	do{
		cout << "Are you ordering drinks? ";
		cin >> ans;
		cout << endl;

		if(!((ans == 'y') || (ans == 'Y') || (ans == 'n') || (ans == 'N'))){
			cout << "Your choices are Y or N" << endl;
			cout << endl;
		}

	}while(!((ans == 'y') || (ans == 'Y') || (ans == 'n') || (ans == 'N')));

	if((ans == 'y') || (ans == 'Y'))
		drink(total, Famount, Bamount, Damount, ans);
	else
		Total(total, Famount, Bamount, Damount, ans);
}

void burger(char ans){
	int Bamount = 0, Damount = 0, Famount = 0;
	double Bcost = 2.5, total;

	do{
		cout << "How many Burgers would you like to order?: ";
		cin >> Bamount;
		cout << endl;

		if(Bamount < 0){
			cout << "You cannot order a negative amount of Burgers!" << endl;
			cout << endl;
		}

	}while(Bamount < 0);

	total = Bcost * Bamount;

	do{
		cout << "Would you like to order a fries? ";
		cin >> ans;
		cout << endl;

		if(!((ans == 'y') || (ans == 'Y') || (ans == 'n') || (ans == 'N'))){
			cout << "Your choices are Y or N" << endl;
			cout << endl;
		}

	}while(!((ans == 'y') || (ans == 'Y') || (ans == 'n') || (ans == 'N')));

	if((ans == 'y') || (ans == 'Y'))
		fries(total, Bamount, Famount, Damount, ans);
	else
		drink(total, Bamount, Famount, Damount, ans);
}

int main(){
	string order;
	char ans;
	cout.setf(ios::fixed);
	cout.precision(2);

	do{
	   clear();

	   	  cout << "Food Order" << endl;
	   	  cout << endl;

	   	  burger(ans);

        do{
           cout << "Would you like to place another order? ";
           cin >> ans;
           cout << endl;

           if(!((ans == 'y') || (ans == 'Y') || (ans == 'n') || (ans == 'N'))){
           			 cout << "Your choices are Y or N" << endl;
           			 cout << endl;
           }

        }while(!((ans == 'y') || (ans == 'Y') || (ans == 'n') || (ans == 'N')));

	}while((ans == 'y') || (ans == 'Y'));

    return(0);
}
