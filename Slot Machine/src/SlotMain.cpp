/*Jon Healy*/
/*Slot Machine*/

#include<iostream>
#include<cstdlib>
#include<ctime>


using namespace std;

void clear()
{int i;
 for(i = 1; i <= 26; i++)
	 cout << endl;
}

int main()
{cout << "Slot Machine" << endl;
 cout << endl;

 int tokens, slot1, slot2, slot3, num1 = 1, num2 = 2, num3 = 3;
 char ans;

 do{
 	clear();
    tokens = 100;

	do{

	    do{
			cout << "You have " << tokens << " tokens. Pull? ";
			cin >> ans;
			cout << endl;

			if(!((ans == 'y') || (ans == 'Y') || (ans == 'n') || (ans == 'N'))){
				cout << "Your choices are Y or N" << endl;
				cout << endl;
			}

			}while(!((ans == 'y') || (ans == 'Y') || (ans == 'n') || (ans == 'N')));
			 tokens = tokens - 1;

			srand(time(NULL));
			slot1 = rand()%(num3 - num1 + 1) + num1;
			slot2 = rand()%(num3 - num1 + 1) + num1;
			slot3 = rand()%(num3 - num1 + 1) + num1;

			if(!(ans == 'n') || (ans == 'N')) {
				cout << "[" << slot1 << "]" << "[" << slot2 << "]" << "[" << slot3 <<"]" << endl;
				cout << endl;
			}

			if((slot1 == 3) && (slot2 == 3) && (slot3 == 3)) {
				tokens = tokens + 13;
				cout << "You won " << 12 << " tokens" << endl;
				cout << endl;
			}

			if((slot1 == 2) && (slot2 == 2) && (slot3 == 2)) {
	            tokens = tokens + 9;
				cout << "You won " << 8 << " tokens" << endl;
				cout << endl;
			}

			if((slot1 == 1) && (slot2 == 1) && (slot3 == 1)) {
				cout << "You won " << 4 << " tokens" << endl;
				tokens = tokens + 5;
				cout << endl;
			}

		}while(((ans == 'y') || (ans == 'Y')) && (!(tokens == 0)));

		if(tokens >= 1){
			cout << "Your token amount for this round is " << tokens << endl;
			cout << endl;
		}

		if(tokens == 0){
			cout << "You have lost" << endl;
			cout << endl;
		}

		do{
	   		cout << "Do you want to run again? ";
	   		cin >> ans;
	   		cout << endl;

	   		if(!((ans == 'y') || (ans == 'Y') || (ans == 'n') || (ans == 'N'))) {
  		   	   cout << "The choices are Y or N " << endl;
 			   cout << endl;
            }

	   	}while(!((ans == 'y') || (ans == 'Y') || (ans == 'n') || (ans == 'N')));

	}while((ans == 'y') || (ans == 'Y'));

cout << "Thanks for playing" << endl;
cout << endl;

return(0); }
